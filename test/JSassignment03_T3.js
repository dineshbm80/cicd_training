//  Variables declared with let or const are not hoisted

//case 01: intialized>used >Declared
a=100;
console.log(a)
var a;

//case 2: Declared>Intialized >Used
var b;
b=200;
console.log(b)

//case 3: Declared>used>intialized
var c;
console.log(c)
c=50;
// out put will be undefined since intalization is done after use

