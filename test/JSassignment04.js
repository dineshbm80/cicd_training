//Task 01:
// Create array like [1, 2, 3, 4, 5 ]
var ar=[1,2,3,4,5]

// Make square of array values by function. Ex [1, 4, 9, 16, 25
var arra=[]
ar.forEach(function(b){

    arra.push(b*b)
})

console.log(arra)

// Make sum of all values in above array. Ex 55

var sum=0;
arra.forEach(function(b){
    sum=sum+b
})
console.log(sum)

// Task 02: Create two dimensional array and iterate each element using forEach.
var items = [[1, 2],[3, 4],[5, 6]];
for(i=0;i<items.length; i++){
    for(j=0;j<items[i].length; j++)
    {
        console.log(items[i][j])
    }
    console.log("\n")
   
}