
//Task 02

//case 01: calling function at the top 
myfunction()

function myfunction()
{
console.log("testing")

}


//case 02: Calling function at the end by intializing it to variable : o/p will be undefined since the value is not assigned to variable while accessing

abc
console.log(abc)
var abc=function tests01()
{
console.log("testing  001")
}



//case 03: Calling function at the end
function tests02()
{
console.log("testing 002")
return 100
}
tests02()